import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from "@ionic-native/social-sharing";
import QRCode from "qrcode";

/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {

  date = null;
  data = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, public socialSharing: SocialSharing) {
    let date = this.navParams.get('date');
    this.date = date;
    let data = this.navParams.get('data');
    this.data = data;
  }

  share() {
    let generatedUrl = null;
    QRCode.toDataURL(this.data, function (err, url) {
      generatedUrl = url;
    });
    this.socialSharing.share(this.data, null, generatedUrl);
  }
}
