import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { QrCodeProvider } from "../../providers/qr-code/qr-code";
import { SocialSharing } from "@ionic-native/social-sharing";
import QRCode from "qrcode";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  qrData = null;
  createdCode = null;
  scannedCode = null;
  isDisabled = true;

  constructor(
      public navCtrl: NavController,
      private barcodeScanner: BarcodeScanner,
      public qrCode: QrCodeProvider,
      public toastCtrl: ToastController,
      public socialSharing: SocialSharing
  ) {

  }

  generateQRCode() {
    this.createdCode = this.qrData;
    this.qrCode.generate(this.qrData).then(result => {
      this.presentToast(result + " : added to historical.");
    });
  }

  check(text) {
    this.isDisabled = text === '' || text === null;
  }

  clearQRCode() {
    this.createdCode = null;
    this.qrData = "";
    this.isDisabled = true;
  }

  scanQRCode() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;
    }, (err) => {
      console.log('Error: ', err);
    });
  }

  share() {
    let generatedUrl = null;
    QRCode.toDataURL(this.createdCode, function (err, url) {
      generatedUrl = url;
    });
    this.socialSharing.share(this.createdCode, null, generatedUrl);
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'bottom',
      showCloseButton: true
    });
    toast.present();
  }

}
