import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { DetailsPage } from "../details/details";

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {

  list = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {

  }

  ionViewWillEnter() {
    this.list = [];
    this.storage.keys().then(keys => {
      for (let key of keys.reverse()) {
        this.storage.get(key).then(data => {
          let date = this.parseDate(key);
          this.list.push({date: date, data: data});
        });
      }
    });
  }

  getDetails(date, data) {
    this.navCtrl.push(DetailsPage, {date: date, data: data});
  }

  parseDate(date) {
    let fullDate = new Date(date);
    let year = fullDate.getFullYear();
    let month = fullDate.getMonth() + 1 < 10 ? "0" + (fullDate.getMonth() + 1) : fullDate.getMonth() + 1;
    let day = fullDate.getDay() - 1 < 10 ? "0" + (fullDate.getDay() - 1) : fullDate.getDay() - 1;
    let hours = fullDate.getHours() < 10 ? "0" + (fullDate.getHours()) : fullDate.getHours();
    let minutes = fullDate.getMinutes() < 10 ? "0" + (fullDate.getMinutes()) : fullDate.getMinutes();
    return day + '/' + month + '/' + year + ' - ' + hours + ":" + minutes;
  }
}
