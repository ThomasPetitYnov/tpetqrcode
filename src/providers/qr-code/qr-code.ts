import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage";

/*
  Generated class for the QrCodeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class QrCodeProvider {

  constructor(public storage: Storage) {

  }

  async generate(text: string): Promise<string> {
    const result = await this.storage.set(new Date().toString(), text);
    return Promise.resolve(result);
  }

}
